Source: stiff
Section: science
Priority: optional
Maintainer: Debian Astronomy Maintainers <debian-astro-maintainers@lists.alioth.debian.org>
Uploaders: Gijs Molenaar <gijs@pythonic.nl>, Ole Streicher <olebole@debian.org>
Build-Depends: debhelper (>= 12),
               dh-autoreconf,
               libjpeg-dev,
               libtiff5-dev,
               zlib1g-dev
Standards-Version: 4.3.0
Homepage: https://www.astromatic.net/software/stiff
Vcs-Git: https://salsa.debian.org/debian-astro-team/stiff.git
Vcs-Browser: https://salsa.debian.org/debian-astro-team/stiff

Package: stiff
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: convert scientific FITS images to the TIFF format
 STIFF is a program that converts scientific FITS images to the TIFF format
 for illustration purposes. The main features of STIFF are:
 .
  * Accurate reproduction of the original surface brightnesses and colours
  * Automatic or manual contrast and brightness adjustments
  * Automatic sky background intensity and colour balance
  * Adjustable colour saturation
  * Colour-friendly gamma correction capabilities
  * One or three input channels: gray-scale or true colour output
  * Output with 8 or 16 bits per component
  * Pixel rebinning and x/y flip options
  * Support for arbitrarily large input and output images on standard
    hardware (BigTIFF support)
  * Support for tiled, multiresolution pyramids
  * Support for lossless and lossy compression methods
  * Multi-threaded code with load-balancing to take advantage of multiple
    cores and processors.
  * XML VOTable-compliant output of meta-data.
